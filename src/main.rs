use std::collections::{HashSet, HashMap, BTreeMap};
use std::io::prelude::*;
use std::slice::Iter;

fn main() {
    let input = read_input().unwrap();
    let (flop, hands) = parse_input(input).unwrap();
    let deck = generate_deck(&flop, &hands);

    let outcomes = (&deck).iter()
        // cartesian product to get all possible Turn + River combinations
        .flat_map(|turn| {
            (&deck).iter()
                .filter(|river| turn != *river)
                .map(|river| (turn.clone(), river.clone()))
                .collect::<Vec<(Card, Card)>>()
        })
        // interesting part: determining the winner for each possibility
        .map(|turn_river| winner(&hands, &flop, turn_river))
        .collect::<Vec<Vec<usize>>>();

    let quotas = calculate_quotas(&outcomes, (&hands).len());

    println!("Ties: {:.*}%", 1, quotas[0] * 100.);
    for (player, quota) in quotas[1..].iter().enumerate() {
        println!("{}: {:.*}%", player + 1, 1, quota * 100.);
    }
}

fn winner(hands: &Vec<Hand>, flop: &Flop, turn_river: (Card, Card)) -> Vec<usize> {
    let mut rankings = hands.iter()
        .map(|hand| {
            let c = Combination::build(hand, flop, &turn_river);
            // interesting part: determining the best 5 cards in a combination
            c.best_5_cards()
        })
        .enumerate()
        .collect::<Vec<(usize, Ranking)>>();
    rankings.sort_by(|a, b| b.1.cmp(&a.1));

    let mut winners = Vec::new();
    let ref first  = (&rankings)[0];
    winners.push(first.0);

    rankings.iter().skip(1)
        .take_while(|&&(_, ref r)| *r == first.1)
        .fold((), |_, &(i, _)| { winners.push(i); () });

    winners
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Suit { Clubs, Spades, Diamonds, Hearts }

impl Suit {
    fn all() -> Iter<'static, Suit> {
        static SUITS: [Suit; 4] = [Suit::Clubs, Suit::Spades,
                                   Suit::Diamonds, Suit::Hearts];
        SUITS.into_iter()
    }
}

impl std::str::FromStr for Suit {
    type Err = ();

    fn from_str(s: &str) -> Result<Suit, Self::Err> {
        match s {
            "C" => Ok(Suit::Clubs),
            "S" => Ok(Suit::Spades),
            "D" => Ok(Suit::Diamonds),
            "H" => Ok(Suit::Hearts),
            _ => Err(())
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Value { Two, Three, Four, Five, Six, Seven, Eight,
             Nine, Ten, Jack, Queen, King, Ace }

impl Value {
    fn all() -> Iter<'static, Value> {
        static VALUES: [Value; 13] = [Value::Two, Value::Three, Value::Four,
                                      Value::Five, Value::Six, Value::Seven,
                                      Value::Eight, Value::Nine, Value::Ten,
                                      Value::Jack, Value::Queen, Value::King,
                                      Value::Ace];
        VALUES.into_iter()
    }

    fn derive_straight(values: Vec<Value>) -> Option<Value> {
        let num_values = (&values).iter()
            .map(|&v| unsafe {
                std::mem::transmute::<Value, u8>(v)
            })
            .collect::<Vec<u8>>();

        let mut max = (0, 0);
        let mut cur = (0, 0);
        let mut prev = 0;
        for (i, v) in num_values.iter().enumerate() {
            if prev == *v + 1 {
                cur.1 += 1;
                // We have to account for the Ace
                // also being used as a virtual One
                if *v == 0 && num_values[0] == 12 {
                    cur.1 += 1;
                }
            } else {
                cur = (i, 1);
            }
            if cur.1 > max.1 {
                max = cur;
            }
            prev = *v;
        }

        if max.1 >= 5 {
            Some(values[max.0])
        } else {
            None
        }
    }
}

impl std::str::FromStr for Value {
    type Err = ();

    fn from_str(s: &str) -> Result<Value, Self::Err> {
        match s {
            "2" => Ok(Value::Two),
            "3" => Ok(Value::Three),
            "4" => Ok(Value::Four),
            "5" => Ok(Value::Five),
            "6" => Ok(Value::Six),
            "7" => Ok(Value::Seven),
            "8" => Ok(Value::Eight),
            "9" => Ok(Value::Nine),
            "0" => Ok(Value::Ten),
            "J" => Ok(Value::Jack),
            "Q" => Ok(Value::Queen),
            "K" => Ok(Value::King),
            "A" => Ok(Value::Ace),
            _ => Err(())
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Card(Value, Suit);

impl std::str::FromStr for Card {
    type Err = ();

    fn from_str(s: &str) -> Result<Card, Self::Err> {
        Ok(Card(s[..1].parse()?, s[1..].parse()?))
    }
}

impl Ord for Card {
    fn cmp(&self, other: &Card) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Card) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug)]
struct Flop {
    cards: [Card; 3]
}

impl std::str::FromStr for Flop {
    type Err = ();

    fn from_str(s: &str) -> Result<Flop, Self::Err> {
        Ok(Flop {
            cards: [s[..2].parse()?, s[2..4].parse()?, s[4..].parse()?]
        })
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Hand {
    cards: [Card; 2]
}

impl std::str::FromStr for Hand {
    type Err = ();

    fn from_str(s: &str) -> Result<Hand, Self::Err> {
        Ok(Hand {
            cards: [s[..2].parse()?, s[2..].parse()?]
        })
    }
}

#[derive(Debug)]
struct Combination<'a> {
    cards: [&'a Card; 7],
    v_counts: BTreeMap<Value, usize>,
    s_counts: HashMap<Suit, usize>
}

impl<'a> Combination<'a> {
    fn build(hand: &'a Hand, flop: &'a Flop, turn_river: &'a (Card, Card)) -> Combination<'a> {
        let mut cards = [&hand.cards[0], &hand.cards[1],
                         &flop.cards[0], &flop.cards[1], &flop.cards[2],
                         &turn_river.0,  &turn_river.1];
        cards.sort_by(|a, b| b.cmp(a));

        let mut v_counts = BTreeMap::new();
        let mut s_counts = HashMap::new();

        for &card in (&cards).iter() {
            let v_count = v_counts.entry(card.0).or_insert(0);
            *v_count += 1;
            let s_count = s_counts.entry(card.1).or_insert(0);
            *s_count += 1;
        }

        Combination { cards: cards, v_counts: v_counts, s_counts: s_counts }
    }

    fn best_5_cards(&'a self) -> Ranking {
        if let Some(r) = self.straight_flush() {
            return r
        }
        if let Some(r) = self.four_of_a_kind() {
            return r
        }
        if let Some(r) = self.full_house() {
            return r
        }
        if let Some(r) = self.flush() {
            return r
        }
        if let Some(r) = self.straight() {
            return r
        }
        if let Some(r) = self.three_of_a_kind() {
            return r
        }
        if let Some(r) = self.two_pair() {
            return r
        }
        if let Some(r) = self.one_pair() {
            return r
        }
        self.high_card()
    }

    fn straight_flush(&'a self) -> Option<Ranking> {
        let mut suits = self.s_counts.iter()
            .filter(|&(_, count)| *count >= 5);

        if let Some((flush, _)) = suits.next() {
            let cards = self.cards.iter()
                .filter(|&card| card.1 == *flush)
                .map(|&card| card.0)
                .collect::<Vec<Value>>();

            if let Some(card) = Value::derive_straight(cards) {
                return Some(Ranking::StraightFlush(card));
            }
        }

        None
    }

    fn four_of_a_kind(&'a self) -> Option<Ranking> {
        let mut quadruplets = self.v_counts.iter().rev()
            .filter(|&(_, count)| *count == 4);

        if let Some((quadruplet, _)) = quadruplets.next() {
            let kicker = self.cards.iter()
                .filter(|card| card.0 != *quadruplet)
                .next()
                .unwrap();

            return Some(Ranking::FourOfAKind(*quadruplet, **kicker));
        }

        None
    }

    fn full_house(&'a self) -> Option<Ranking> {
        let mut triplets = self.v_counts.iter().rev()
            .filter(|&(_, count)| *count == 3);

        if let Some((triplet, _)) = triplets.next() {
            if let Some((other_triplet, _)) = triplets.next() {
                return Some(Ranking::FullHouse(*triplet, *other_triplet));
            } else {
                let mut doublets = self.v_counts.iter().rev()
                    .filter(|&(_, count)| *count == 2);

                if let Some((doublet, _)) = doublets.next() {
                    return Some(Ranking::FullHouse(*triplet, *doublet));
                }
            }
        }

        None
    }

    fn flush(&'a self) -> Option<Ranking> {
        let mut suits = self.s_counts.iter()
            .filter(|&(_, count)| *count >= 5);

        if let Some((flush, _)) = suits.next() {
            let cards = self.cards.iter()
                .filter(|&card| card.1 == *flush)
                .take(5)
                .collect::<Vec<&&Card>>();

            return Some(Ranking::Flush(
                **cards[0],
                **cards[1],
                **cards[2],
                **cards[3],
                **cards[4]
            ));
        }

        None
    }

    fn straight(&'a self) -> Option<Ranking> {
        let cards = self.v_counts.keys().rev()
            .map(|&v| v)
            .collect::<Vec<Value>>();

        if let Some(card) = Value::derive_straight(cards) {
            return Some(Ranking::Straight(card));
        }

        None
    }

    fn three_of_a_kind(&'a self) -> Option<Ranking> {
        let mut triplets = self.v_counts.iter().rev()
            .filter(|&(_, count)| *count == 3);

        if let Some((triplet, _)) = triplets.next() {
            let kickers = self.cards.iter()
                .filter(|card| card.0 != *triplet)
                .take(2)
                .collect::<Vec<&&Card>>();

            return Some(Ranking::ThreeOfAKind(*triplet, **kickers[0], **kickers[1]));
        }

        None
    }

    fn two_pair(&'a self) -> Option<Ranking> {
        let mut doublets = self.v_counts.iter().rev()
            .filter(|&(_, count)| *count == 2);

        if let Some((doublet, _)) = doublets.next() {
            if let Some((other_doublet, _)) = doublets.next() {
                let kicker = self.cards.iter()
                    .filter(|card| card.0 != *doublet && card.0 != *other_doublet)
                    .next()
                    .unwrap();

                return Some(Ranking::TwoPair(*doublet, *other_doublet, **kicker));
            }
        }

        None
    }

    fn one_pair(&'a self) -> Option<Ranking> {
        let mut doublets = self.v_counts.iter().rev()
            .filter(|&(_, count)| *count == 2);

        if let Some((doublet, _)) = doublets.next() {
            let kickers = self.cards.iter()
                .filter(|card| card.0 != *doublet)
                .take(3)
                .collect::<Vec<&&Card>>();

            return Some(Ranking::OnePair(*doublet, **kickers[0], **kickers[1], **kickers[2]));
        }

        None
    }

    fn high_card(&'a self) -> Ranking {
        Ranking::HighCard(
            *self.cards[0],
            *self.cards[1],
            *self.cards[2],
            *self.cards[3],
            *self.cards[4]
        )
    }
}

// Applying the "Best 5 Cards" rule
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum Ranking {
    HighCard(Card, Card, Card, Card, Card), // 5 kickers
    OnePair(Value, Card, Card, Card),       // pair, 3 kickers
    TwoPair(Value, Value, Card),            // top pair, lower pair, kicker
    ThreeOfAKind(Value, Card, Card),        // triplet, 2 kickers
    Straight(Value),                        // highest card of the straight
    Flush(Card, Card, Card, Card, Card),    // 5 kickers of the same color
    FullHouse(Value, Value),                // triplet, doublet
    FourOfAKind(Value, Card),               // quadruplet, kicker
    StraightFlush(Value)                    // highest card of the straight flush
                                            // RoyalFlush := StraigthFlush(Ace)
}

fn generate_deck(flop: &Flop, hands: &Vec<Hand>) -> HashSet<Card> {
    let mut deck = HashSet::new();
    for suit in Suit::all() {
        for value in Value::all() {
            deck.insert(Card(*value, *suit));
        }
    }

    for card in &(flop.cards) {
        assert_eq!(deck.remove(&card), true);
    }
    for hand in hands {
        for card in &(hand.cards) {
            assert_eq!(deck.remove(&card), true);
        }
    }

    deck
}

fn calculate_quotas(outcomes: &Vec<Vec<usize>>, players: usize) -> Vec<f64> {
    let wins = outcomes.iter()
        .fold(vec![0; players + 1], |mut acc, outcome| {
            // tie
            if (&outcome).len() > 1 {
                acc[0] += 1;
            } else {
                for &winner in outcome {
                    acc[winner+1] += 1;
                }
            }
            acc
        });
    wins.iter()
        .map(|&winner| winner as f64 / outcomes.len() as f64)
        .collect()
}

fn read_input() -> std::io::Result<String> {
    let mut buf = String::new();
    std::io::stdin().read_to_string(&mut buf)?;
    Ok(buf)
}

fn parse_input(input: String) -> Result<(Flop, Vec<Hand>), ()> {
    let flop = input[0..6].parse::<Flop>()?;
    let hands_rep = input.split_whitespace().skip(1);
    let mut hands = Vec::new();
    for s in hands_rep {
        let hand = s.parse::<Hand>()?;
        hands.push(hand);
    }
    Ok((flop, hands))
}
